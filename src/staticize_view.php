<?php

return [
    'disabled' => env('STATICIZE_VIEW_DISABLED', false),
];
